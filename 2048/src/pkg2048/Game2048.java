/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2048;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Stack;
import javax.swing.JOptionPane;

/**
 *
 * @author My Comp
 */
public class Game2048 extends javax.swing.JFrame implements KeyListener{

   double b;
        int block,highscore;
        int score=0;
        String s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16;
        boolean chance=false,check=false,semaphore=true,xxx=false,starter=false;
        boolean flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false,flag11=false,flag12=false,flag13=false,flag14=false,flag15=false,flag16=false;
        
       // User1 online;
        
        Stack<String> stack1=new Stack<>();
        Stack<String> stack2=new Stack<>();
        Stack<String> stack3=new Stack<>();
        Stack<String> stack4=new Stack<>();
        Stack<String> stack5=new Stack<>();
        Stack<String> stack6=new Stack<>();
        Stack<String> stack7=new Stack<>();
        Stack<String> stack8=new Stack<>();
        Stack<String> stack9=new Stack<>();
        Stack<String> stack10=new Stack<>();
        Stack<String> stack11=new Stack<>();
        Stack<String> stack12=new Stack<>();
        Stack<String> stack13=new Stack<>();
        Stack<String> stack14=new Stack<>();
        Stack<String> stack15=new Stack<>();
        Stack<String> stack16=new Stack<>();
        Stack<Integer> sc=new Stack<>();
        
        
   
    public Game2048() {
        initComponents();
         setLocation(500,50);
         left.addKeyListener(this);
         right.addKeyListener(this);
          up.addKeyListener(this);
           down.addKeyListener(this);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    void color(javax.swing.JTextField t)
        {
          switch (t.getText()) {
              case "":
                  t.setBackground(new Color(255,255,255));
                  break;
              case "2": t.setBackground(new Color(255, 255, 217));
                  break;
              case "4":
                   t.setBackground(new Color(255, 255, 191));
                  break;
              case "8":
                  t.setBackground(new Color(255, 255, 145));
                  break;
              case "16": 
                  t.setBackground(new Color(255, 229, 147));
                  break;
              case "32":
              case "64":
                  t.setBackground(new Color(255, 255, 204));
                  break;
              case "128":
              case "256":
                  t.setBackground(new Color(255, 229, 204));
                  break;
              case "512":
                  t.setBackground(new Color(229, 255, 204));
                  break;
              case "1024":
                  t.setBackground(new Color(153, 255, 153));
                  break;
              case "2048":
                  t.setBackground(new Color(102, 255, 102));
                  break;
              case "4096":
                  t.setBackground(new Color(51, 255, 255));
                  break;
              case "8192":
                  t.setBackground(new Color(0, 128, 255));
                  break;
              case "16384":
                  t.setBackground(new Color(178, 102, 255));
                  break;
              case "32768":
                  t.setBackground(new Color(204, 0, 102));
                  break;
              default:
                  t.setBackground(new Color(0, 0,204));
                  break;
          }

        }
        
        void colorcombination()
        {
            color(b1); color(b2); color(b3); color(b4); color(b5); color(b6); color(b7); color(b8);
             color(b9); color(b10); color(b11); color(b12); color(b13); color(b14); color(b15); color(b16);
        }
        void saveButtons()
        {
            stack1.push(b1.getText());
            stack2.push(b2.getText());
            stack3.push(b3.getText());
            stack4.push(b4.getText());
            stack5.push(b5.getText());
            stack6.push(b6.getText());
            stack7.push(b7.getText());
            stack8.push(b8.getText());
            stack9.push(b9.getText());
            stack10.push(b10.getText());
            stack11.push(b11.getText());
            stack12.push(b12.getText());
            stack13.push(b13.getText());
            stack14.push(b14.getText());
            stack15.push(b15.getText());
            stack16.push(b16.getText());
            sc.push(score);
          }
        void loadButtons()
        {
            b1.setText(stack1.pop());
            b2.setText(stack2.pop());
            b3.setText(stack3.pop());
            b4.setText(stack4.pop());
            b5.setText(stack5.pop());
            b6.setText(stack6.pop());
            b7.setText(stack7.pop());
            b8.setText(stack8.pop());
            b9.setText(stack9.pop());
            b10.setText(stack10.pop());
            b11.setText(stack11.pop());
            b12.setText(stack12.pop());
            b13.setText(stack13.pop());
            b14.setText(stack14.pop());
            b15.setText(stack15.pop());
            b16.setText(stack16.pop());
            score=sc.pop();
            txtscore.setText(String.valueOf(score));
            
            
            colorcombination();
            
            
        }
        boolean equality(javax.swing.JTextField b1,javax.swing.JTextField b2,javax.swing.JTextField b3,javax.swing.JTextField b4)
        {
            return b1.getText().equals(b2.getText())||b3.getText().equals(b2.getText())||b3.getText().equals(b4.getText());
        }
         boolean equality(javax.swing.JTextField b1,javax.swing.JTextField b2,javax.swing.JTextField b3)
        {
            return b1.getText().equals(b2.getText())||b3.getText().equals(b2.getText());
        }
          boolean equality(javax.swing.JTextField b1,javax.swing.JTextField b2)
        {
            return b1.getText().equals(b2.getText());
        }
        boolean checkeradvance(javax.swing.JTextField b1,javax.swing.JTextField b2,javax.swing.JTextField b3,javax.swing.JTextField b4)
        {
            return ((!b1.getText().equals(""))&&(!b2.getText().equals(""))&&(!b3.getText().equals(""))&&(!b4.getText().equals(""))&&(!equality(b1,b2,b3,b4)))||((b1.getText().equals(""))&&(!b2.getText().equals(""))&&(!b3.getText().equals(""))&&(!b4.getText().equals(""))&&(!equality(b2,b3,b4)))||((b1.getText().equals(""))&&(b2.getText().equals(""))&&(!b3.getText().equals(""))&&(!b4.getText().equals(""))&&(!equality(b3,b4)))||(b1.getText().equals("")&&(b2.getText().equals(""))&&(b3.getText().equals(""))&&(!b4.getText().equals("")))||(b1.getText().equals("")&&(b2.getText().equals(""))&&(b3.getText().equals(""))&&(b4.getText().equals("")));
        }
        boolean checker(javax.swing.JTextField b1,javax.swing.JTextField b2,javax.swing.JTextField b3,javax.swing.JTextField b4,javax.swing.JTextField b5,javax.swing.JTextField b6,javax.swing.JTextField b7,javax.swing.JTextField b8,javax.swing.JTextField b9,javax.swing.JTextField b10,javax.swing.JTextField b11,javax.swing.JTextField b12,javax.swing.JTextField b13,javax.swing.JTextField b14,javax.swing.JTextField b15,javax.swing.JTextField b16)
        {
            return checkeradvance(b1,b2,b3,b4)&&checkeradvance(b5,b6,b7,b8)&&checkeradvance(b9,b10,b11,b12)&&checkeradvance(b13,b14,b15,b16);
        }
        
        void logic(javax.swing.JTextField b1,javax.swing.JTextField b2,javax.swing.JTextField b3,javax.swing.JTextField b4)
        {
            //condition one if 3 is filled----1:
        if (!b3.getText().equals(""))
        {
            //1.1
           
             if (b4.getText().equals(""))
            {
                 //1.1.1
                if (b2.getText().equals("")&&b1.getText().equals(""))
                    {
                     b4.setText(b3.getText());
                      b3.setText("");
                      
                     }
                //1.1.2
                else if ((!b2.getText().equals(""))&&b1.getText().equals(""))
                    {
                        if (b2.getText().equals(b3.getText()))
                        {score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b3.getText());
                            b4.setText(String.valueOf(Integer.parseInt(b2.getText())+Integer.parseInt(b3.getText())));
                            
                            b3.setText("");
                            b2.setText("");
                            
                        }
                        else
                        {
                            b4.setText(b3.getText());
                            b3.setText(b2.getText());
                            b2.setText("");
                        }
                    }
                //1.1.3
                else if ((!b1.getText().equals(""))&&b2.getText().equals(""))
                    {
                         if (b1.getText().equals(b3.getText()))
                        {score=score+Integer.parseInt(b1.getText())+Integer.parseInt(b3.getText());
                         b4.setText(String.valueOf(Integer.parseInt(b1.getText())+Integer.parseInt(b3.getText()))); 
                         
                         b1.setText("");
                         b2.setText("");
                         b3.setText("");
                         }
                         else
                        {
                        b4.setText(b3.getText());
                        b3.setText(b2.getText());
                        b2.setText("");
                        }
            
                    }
                //1.1.4
                else if ((!b1.getText().equals(""))&&(!b2.getText().equals("")))
                    {
                         if (b3.getText().equals(b2.getText()))
                       { score=score+Integer.parseInt(b3.getText())+Integer.parseInt(b2.getText());
                         b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b2.getText())));
                        
                         b3.setText(b1.getText());
                         b1.setText("");
                         b2.setText("");
                        }
                         else if (b1.getText().equals(b2.getText()))
                        {
                          b4.setText(b3.getText());
                          score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b1.getText());
                         b3.setText(String.valueOf(Integer.parseInt(b1.getText())+Integer.parseInt(b2.getText())));
                         
                         b1.setText("");
                         b2.setText("");
                        }
                         else
                         {
                             b4.setText(b3.getText());
                              b3.setText(b2.getText());
                               b2.setText(b1.getText());
                               b1.setText("");
                         }
                    }
             }   //end of 1.1
        //starting of 1.2
           else if (b2.getText().equals(""))
           {
               //1.2.1
               if ((!b4.getText().equals(""))&&b1.getText().equals(""))
                       {
                           if (b3.getText().equals(b4.getText()))
                           {score=score+Integer.parseInt(b4.getText())+Integer.parseInt(b3.getText());
                               b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b4.getText())));
                               
                               b3.setText("");
                           }
                           
                       }
               //1.2.2
               else if ((!b1.getText().equals(""))&&b4.getText().equals(""))
                        {
                            if (b3.getText().equals(b1.getText()))
                            {score=score+Integer.parseInt(b3.getText())+Integer.parseInt(b1.getText());
                                b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b1.getText())));
                                
                                b1.setText("");
                                b3.setText("");
                            }   
                            else
                            {
                                b4.setText(b3.getText());
                                b3.setText(b1.getText());
                                b1.setText("");
                            }
                        }
               //1.2.3
               else if ((!b1.getText().equals(""))&&(!b4.getText().equals("")))
                        {
                                if (b4.getText().equals(b3.getText()))
                                {score=score+Integer.parseInt(b4.getText())+Integer.parseInt(b3.getText());
                                    b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b4.getText())));
                                    
                                     b3.setText(b1.getText());
                                    b1.setText("");
                                }
                                else if (b1.getText().equals(b3.getText()))
                                { score=score+Integer.parseInt(b1.getText())+Integer.parseInt(b3.getText());
                                    b3.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b1.getText())));
                                   
                                    b1.setText("");
                                }
                                else 
                                {
                                    b2.setText(b1.getText());
                                    b1.setText("");
                                }
                                
                        }//end of 1.2
           }//starting of 1.3
              //1.3 
           else if (b1.getText().equals(""))
           {
               //1.3.1
                  if (((!b2.getText().equals(""))&&(!b4.getText().equals(""))))
                  {
                      if (b4.getText().equals(b3.getText()))
                      {score=score+Integer.parseInt(b4.getText())+Integer.parseInt(b3.getText());
                           b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b4.getText())));
                           
                           b3.setText(b2.getText());
                           b2.setText("");
                      }
                      else if (b2.getText().equals(b3.getText()))
                          {score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b3.getText());
                           b3.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b2.getText())));
                           
                          
                           b2.setText("");
                         }
                  }//end of 1.3
           }
           
       }
      
                
        //condition 2: when all 4 blocks are filled
        if ((!b1.getText().equals(""))&&(!b2.getText().equals(""))&&(!b3.getText().equals(""))&&(!b4.getText().equals("")))
            {
                    if (b1.getText().equals(b2.getText())&&b3.getText().equals(b4.getText()))
                    {
                        score=score+Integer.parseInt(b3.getText())+Integer.parseInt(b4.getText());
                        b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b4.getText())));
                        score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b1.getText());
                        b3.setText(String.valueOf(Integer.parseInt(b1.getText())+Integer.parseInt(b2.getText())));
                        
                         b1.setText("");
                         b2.setText("");
                    }
                    else if (b4.getText().equals(b3.getText()))
                    {score=score+Integer.parseInt(b4.getText())+Integer.parseInt(b3.getText());
                         b4.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b4.getText())));
                         
                         b3.setText(b2.getText());
                         b2.setText(b1.getText());
                         b1.setText("");
                    }
                    else if (b2.getText().equals(b3.getText()))
                    {score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b3.getText());
                        b3.setText(String.valueOf(Integer.parseInt(b3.getText())+Integer.parseInt(b2.getText())));
                        
                        b2.setText(b1.getText());
                        b1.setText("");
                    }
                    else if (b2.getText().equals(b1.getText()))
                    { score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b1.getText());
                        b2.setText(String.valueOf(Integer.parseInt(b1.getText())+Integer.parseInt(b2.getText())));
                       
                         b1.setText("");
                    }
            }
        //condition Third all when 3 is not filled
        if (b3.getText().equals(""))
        {
            //1
                if (b1.getText().equals("")&&b4.getText().equals("")&&(!b2.getText().equals("")))
                        {
                            b4.setText(b2.getText());
                            b2.setText("");
                       }
                //2
                else if (b2.getText().equals("")&&b4.getText().equals("")&&(!b1.getText().equals("")))
                        {
                            b4.setText(b1.getText());
                            b1.setText("");
                      }
                //3
                else if (!(b2.getText().equals(""))&&b1.getText().equals("")&&(!b4.getText().equals("")))
                {
                    if (b2.getText().equals(b4.getText()))
                    {score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b4.getText());
                        b4.setText(String.valueOf(Integer.parseInt(b4.getText())+Integer.parseInt(b2.getText())));
                        
                        b2.setText("");
                    }
                    else
                    {
                        b3.setText(b2.getText());
                        b2.setText("");
                    }
                }
                //4
                 else if (!(b1.getText().equals(""))&&b2.getText().equals("")&&(!b4.getText().equals("")))
                 {
                      if (b1.getText().equals(b4.getText()))
                      {score=score+Integer.parseInt(b4.getText())+Integer.parseInt(b1.getText());
                          b4.setText(String.valueOf(Integer.parseInt(b4.getText())+Integer.parseInt(b1.getText())));
                          
                          b1.setText("");
                      }
                       else
                    {
                        b3.setText(b1.getText());
                        b1.setText("");
                    }
                 }
                 //5
                else if (!(b1.getText().equals(""))&&b4.getText().equals("")&&(!b2.getText().equals("")))
                 {
                      if (b1.getText().equals(b2.getText()))
                      {score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b1.getText());
                          b4.setText(String.valueOf(Integer.parseInt(b1.getText())+Integer.parseInt(b2.getText())));
                          
                          b1.setText("");
                          b2.setText("");
                      }
                       else
                    {
                        b4.setText(b2.getText());
                        b3.setText(b1.getText());
                         b1.setText("");
                          b2.setText("");
                    }
                 }
                //6
               else if (!(b1.getText().equals(""))&&(!b4.getText().equals(""))&&(!b2.getText().equals("")))
               {
                   if (b4.getText().equals(b2.getText()))
                   { score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b4.getText());
                       b4.setText(String.valueOf(Integer.parseInt(b4.getText())+Integer.parseInt(b2.getText())));
                      
                       b3.setText(b1.getText());
                       b2.setText("");
                       b1.setText("");
                   }
                   else if (b1.getText().equals(b2.getText()))
                   {score=score+Integer.parseInt(b2.getText())+Integer.parseInt(b1.getText());
                       b3.setText(String.valueOf(Integer.parseInt(b1.getText())+Integer.parseInt(b2.getText())));
                       
                      
                       b2.setText("");
                       b1.setText("");
                   }
                   else
                   {
                       b3.setText(b2.getText());
                       b2.setText(b1.getText());
                       b1.setText("");
                   }
               }
        }
  
            
        }
        void fill()
        {
            b=Math.random();
           block=(int)((((b*100)%16)+1));
        }
        
        void filler()
        {
            if (flag1==false||flag2==false||flag3==false||flag4==false||flag5==false||flag6==false||flag7==false||flag8==false||flag9==false||flag10==false||flag11==false||flag12==false||flag13==false||flag14==false||flag15==false||flag16==false)
            {
                fill();
               if (block==1)
               {
                   if (b1.getText().equals(""))
                   {
                       chance=true;
                   b1.setText("2");
                   }
                   else 
                   { flag1=true;
                   filler();
                  
                   }
               }
               else if (block==2)
               {
                  if (b2.getText().equals(""))
                  {
                       chance=true;
                   b2.setText("2");
                   }
                   else 
                   {flag2=true;
                   filler();
                   
                   }
               }
                else if (block==3)
                {
                    if (b3.getText().equals(""))
                    {chance=true;
                   b3.setText("2");}
                   else 
                   {flag3=true;
                   filler();
                   
                   }
                }
               else if (block==4)
                    {
                    if (b4.getText().equals(""))
                    { b4.setText("2");
                    chance=true;}
                   else 
                   {flag4=true;
                   filler();
                   
                   }
                }
               else if (block==5)
                    {
                    if (b5.getText().equals(""))
                    {b5.setText("2");
                    chance=true;}
                   else 
                   { flag5=true;
                   filler();
                  
                   }
                }
                else if (block==6)
                     {
                    if (b6.getText().equals(""))
                    {b6.setText("2");
                    chance=true;}
                   else 
                   { flag6=true;
                   filler();
                  
                   }
                }
               else if (block==7)
                    {
                    if (b7.getText().equals(""))
                    {b7.setText("2");chance=true;}
                   else 
                   {flag7=true;
                   filler();
                   
                   }
                }
               else if (block==8)
                   {
                    if (b8.getText().equals(""))
                    { b8.setText("2");chance=true;}
                   else 
                   { flag8=true;
                   filler();
                  
                   }
                }
               else if (block==9)
                    {
                    if (b9.getText().equals(""))
                    {  b9.setText("2");chance=true;}
                   else 
                   { flag9=true;
                   filler();
                  
                   }
                }
               else if (block==10)
                    {
                    if (b10.getText().equals(""))
                    {b10.setText("2");chance=true;}
                   else 
                   {flag10=true;
                   filler();
                   
                   }
                }
                else if (block==11)
                     {
                    if (b11.getText().equals(""))
                    { b11.setText("2");chance=true;}
                   else 
                   {flag11=true;
                   filler();
                   
                   }
                }
               else if (block==12)
                    {
                    if (b12.getText().equals(""))
                    {   b12.setText("2");chance=true;}
                   else 
                  {flag12=true;
                   filler();
                   
                   }
                }
               else if (block==13)
                    {
                    if (b13.getText().equals(""))
                    { b13.setText("2");chance=true;}
                   else 
                   {flag13=true;
                   filler();
                   
                   }
                }
               else if (block==14)
                   {
                    if (b14.getText().equals(""))
                    { b14.setText("2");chance=true;}
                   else 
                    { flag14=true;
                   filler();
                  
                   }
                }
               else if (block==15)
                    {
                    if (b15.getText().equals(""))
                    {  b15.setText("2");chance=true;}
                   else 
                   {flag15=true;
                   filler();
                   
                   }
                }
                else if (block==16)
                    {
                    if (b16.getText().equals(""))
                    { b16.setText("2");chance=true;}
                   else 
                   {flag16=true;
                   filler();
                   
                   }
                }
               
                
                
                
                
            }
            else
                jTextField1.setText("GAME OVER");
            
            if (chance==true)
            {
               flag1=false;flag2=false;flag3=false;flag4=false;flag5=false;flag6=false;flag7=false;flag8=false;flag9=false;flag10=false;flag11=false;flag12=false;flag13=false;flag14=false;flag15=false;flag16=false;
            }
        
        }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        b1 = new javax.swing.JTextField();
        b2 = new javax.swing.JTextField();
        b3 = new javax.swing.JTextField();
        b4 = new javax.swing.JTextField();
        b5 = new javax.swing.JTextField();
        b6 = new javax.swing.JTextField();
        b7 = new javax.swing.JTextField();
        b8 = new javax.swing.JTextField();
        b9 = new javax.swing.JTextField();
        b10 = new javax.swing.JTextField();
        b11 = new javax.swing.JTextField();
        b12 = new javax.swing.JTextField();
        b13 = new javax.swing.JTextField();
        b14 = new javax.swing.JTextField();
        b15 = new javax.swing.JTextField();
        b16 = new javax.swing.JTextField();
        txtscore = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        undo = new javax.swing.JButton();
        reset = new javax.swing.JButton();
        reset1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        left = new javax.swing.JButton();
        Start = new javax.swing.JButton();
        right = new javax.swing.JButton();
        down = new javax.swing.JButton();
        up = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 51));

        jLabel1.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        jLabel1.setText("Score");
        jLabel1.setIconTextGap(8);

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));
        jPanel3.setLayout(new java.awt.GridLayout(4, 4, 5, 5));

        b1.setEditable(false);
        b1.setBackground(new java.awt.Color(255, 255, 255));
        b1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b1);

        b2.setEditable(false);
        b2.setBackground(new java.awt.Color(255, 255, 255));
        b2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b2);

        b3.setEditable(false);
        b3.setBackground(new java.awt.Color(255, 255, 255));
        b3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b3);

        b4.setEditable(false);
        b4.setBackground(new java.awt.Color(255, 255, 255));
        b4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        b4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b4ActionPerformed(evt);
            }
        });
        jPanel3.add(b4);

        b5.setEditable(false);
        b5.setBackground(new java.awt.Color(255, 255, 255));
        b5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b5);

        b6.setEditable(false);
        b6.setBackground(new java.awt.Color(255, 255, 255));
        b6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b6);

        b7.setEditable(false);
        b7.setBackground(new java.awt.Color(255, 255, 255));
        b7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b7);

        b8.setEditable(false);
        b8.setBackground(new java.awt.Color(255, 255, 255));
        b8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b8);

        b9.setEditable(false);
        b9.setBackground(new java.awt.Color(255, 255, 255));
        b9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b9);

        b10.setEditable(false);
        b10.setBackground(new java.awt.Color(255, 255, 255));
        b10.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b10);

        b11.setEditable(false);
        b11.setBackground(new java.awt.Color(255, 255, 255));
        b11.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b11);

        b12.setEditable(false);
        b12.setBackground(new java.awt.Color(255, 255, 255));
        b12.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b12);

        b13.setEditable(false);
        b13.setBackground(new java.awt.Color(255, 255, 255));
        b13.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jPanel3.add(b13);

        b14.setEditable(false);
        b14.setBackground(new java.awt.Color(255, 255, 255));
        b14.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b14);

        b15.setEditable(false);
        b15.setBackground(new java.awt.Color(255, 255, 255));
        b15.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b15);

        b16.setEditable(false);
        b16.setBackground(new java.awt.Color(255, 255, 255));
        b16.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel3.add(b16);

        txtscore.setFont(new java.awt.Font("Arial", 3, 14)); // NOI18N
        txtscore.setText("0");
        txtscore.setIconTextGap(8);

        undo.setText("Undo");
        undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoActionPerformed(evt);
            }
        });
        jPanel5.add(undo);

        reset.setText("Restart");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });
        jPanel5.add(reset);

        reset1.setText("Go Back");
        reset1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reset1ActionPerformed(evt);
            }
        });
        jPanel5.add(reset1);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("2048");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        left.setText("L");
        left.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leftActionPerformed(evt);
            }
        });

        Start.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Start.setForeground(new java.awt.Color(204, 0, 0));
        Start.setText("S");
        Start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StartActionPerformed(evt);
            }
        });

        right.setText("R");
        right.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rightActionPerformed(evt);
            }
        });

        down.setText("D");
        down.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downActionPerformed(evt);
            }
        });

        up.setText("U");
        up.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(left, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(up, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(down, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(Start, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(right, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(up, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(left, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Start, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(right, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(down, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTextField1.setEditable(false);
        jTextField1.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtscore, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtscore)
                            .addComponent(jLabel1))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_b4ActionPerformed

    private void undoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoActionPerformed
        loadButtons();        // TODO add your handling code here:
    }//GEN-LAST:event_undoActionPerformed

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
        int x= JOptionPane.showConfirmDialog(this,"Do You Want To Restart!!","Warning",JOptionPane.YES_NO_OPTION);
        if (x==JOptionPane.YES_OPTION)
        {
            b1.setText("");
            b11.setText("");b12.setText("");b13.setText("");b14.setText("");b15.setText("");b16.setText("");b10.setText("");
            b2.setText("");b3.setText("");b4.setText("");b5.setText("");b6.setText("");b7.setText("");b8.setText("");b9.setText("");
            chance=false;check=false;semaphore=false;xxx=false;starter=false;
            flag1=false;flag2=false;flag3=false;flag4=false;flag5=false;flag6=false;flag7=false;flag8=false;flag9=false;flag10=false;flag11=false;flag12=false;flag13=false;flag14=false;flag15=false;flag16=false;
            jTextField1.setText("Game Restarted");
            txtscore.setText(String.valueOf(score));
            score=0;
            txtscore.setText("0");

            colorcombination();

        }

        // TODO add your handling code here:
    }//GEN-LAST:event_resetActionPerformed

    private void reset1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reset1ActionPerformed
        int x=JOptionPane.showConfirmDialog(this, "Are You Sure", "Warinng",JOptionPane.YES_NO_OPTION);
        if (x==JOptionPane.YES_OPTION)
        {

            dispose();
        }// TODO add your handling code here:
    }//GEN-LAST:event_reset1ActionPerformed

    private void leftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leftActionPerformed
        if (starter ==true)
        {
            if ((!checker(b4,b3,b2,b1,b8,b7,b6,b5,b12,b11,b10,b9,b16,b15,b14,b13))&&xxx==false)
            semaphore=false;
            saveButtons();
            logic(b4,b3,b2,b1);
            logic(b8,b7,b6,b5);
            logic(b12,b11,b10,b9);
            logic(b16,b15,b14,b13);
            txtscore.setText(String.valueOf(score));
           

            if (semaphore==false)
            {
                filler();
                semaphore=true;
            }
            colorcombination();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_leftActionPerformed

    private void StartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StartActionPerformed
        if (starter==false)
        {
            System.out.println("dhgkdgrlk   ");
            jTextField1.setText("START");
            filler();
            colorcombination();

            starter=true;
        }          // TODO add your handling code here:
    }//GEN-LAST:event_StartActionPerformed

    private void rightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rightActionPerformed
        if (starter==true)
        {
            if ((!checker(b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16))&&(xxx==false))
            semaphore=false;
            saveButtons();
            logic(b1,b2,b3,b4);

            logic(b5,b6,b7,b8);
            logic(b9,b10,b11,b12);
            logic(b13,b14,b15,b16);
            txtscore.setText(String.valueOf(score));
            

            if (semaphore==false)
            {
                filler();
                semaphore=true;
            }
            colorcombination();
        }         // TODO add your handling code here:
    }//GEN-LAST:event_rightActionPerformed

    private void downActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downActionPerformed
        if (starter==true)
        {
            if ((!checker(b4,b8,b12,b16,b3,b7,b11,b15,b2,b6,b10,b14,b1,b5,b9,b13))&&xxx==false)
            semaphore=false;
            saveButtons();
            logic(b4,b8,b12,b16);
            logic(b3,b7,b11,b15);
            logic(b2,b6,b10,b14);
            logic(b1,b5,b9,b13);
            txtscore.setText(String.valueOf(score));

           

            if (semaphore==false)
            {
                filler();
                semaphore=true;
            }
            colorcombination();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_downActionPerformed

    private void upActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upActionPerformed
        if (starter==true)
        {
            if ((!checker(b13,b9,b5,b1,b14,b10,b6,b2,b15,b11,b7,b3,b16,b12,b8,b4))&&xxx==false)
            semaphore=false;
            saveButtons();
            logic(b13,b9,b5,b1);
            logic(b14,b10,b6,b2);
            logic(b15,b11,b7,b3);
            logic(b16,b12,b8,b4);
            txtscore.setText(String.valueOf(score));
            
            if (semaphore==false)
            {
                filler();
                semaphore=true;
            }
            colorcombination();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_upActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Game2048.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Game2048.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Game2048.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Game2048.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Game2048().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Start;
    private javax.swing.JTextField b1;
    private javax.swing.JTextField b10;
    private javax.swing.JTextField b11;
    private javax.swing.JTextField b12;
    private javax.swing.JTextField b13;
    private javax.swing.JTextField b14;
    private javax.swing.JTextField b15;
    private javax.swing.JTextField b16;
    private javax.swing.JTextField b2;
    private javax.swing.JTextField b3;
    private javax.swing.JTextField b4;
    private javax.swing.JTextField b5;
    private javax.swing.JTextField b6;
    private javax.swing.JTextField b7;
    private javax.swing.JTextField b8;
    private javax.swing.JTextField b9;
    private javax.swing.JButton down;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JButton left;
    private javax.swing.JButton reset;
    private javax.swing.JButton reset1;
    private javax.swing.JButton right;
    private javax.swing.JLabel txtscore;
    private javax.swing.JButton undo;
    private javax.swing.JButton up;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
         int x=e.getExtendedKeyCode();
        if (x==KeyEvent.VK_LEFT)
        {
            if (starter ==true)
        {
            if ((!checker(b4,b3,b2,b1,b8,b7,b6,b5,b12,b11,b10,b9,b16,b15,b14,b13))&&xxx==false)
            semaphore=false;
            saveButtons();
            logic(b4,b3,b2,b1);
            logic(b8,b7,b6,b5);
            logic(b12,b11,b10,b9);
            logic(b16,b15,b14,b13);
            txtscore.setText(String.valueOf(score));
           

            if (semaphore==false)
            {
                filler();
                semaphore=true;
            }
            colorcombination();
        }   
        }
        
        else if (x==KeyEvent.VK_RIGHT) 
        {
             if (starter==true)
            {
                    if ((!checker(b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16))&&(xxx==false))
                    semaphore=false;
                    saveButtons();
                    logic(b1,b2,b3,b4);

                    logic(b5,b6,b7,b8);
                    logic(b9,b10,b11,b12);
                    logic(b13,b14,b15,b16);
                    txtscore.setText(String.valueOf(score));
                 
                    if (semaphore==false)
                    {
                        filler();
                        semaphore=true;
                    }
                    colorcombination();
                }      
            
        }
        
        else if (x==KeyEvent.VK_UP)
        {
                        if (starter==true)
                    {
                       if ((!checker(b13,b9,b5,b1,b14,b10,b6,b2,b15,b11,b7,b3,b16,b12,b8,b4))&&xxx==false)
                       semaphore=false;
                       saveButtons();
                       logic(b13,b9,b5,b1);
                       logic(b14,b10,b6,b2);
                       logic(b15,b11,b7,b3);
                       logic(b16,b12,b8,b4);
                       txtscore.setText(String.valueOf(score));
                      
                       if (semaphore==false)
                       {
                           filler();
                           semaphore=true;
                       }
                       colorcombination();
                   }    
            }
        
        else if (x==KeyEvent.VK_DOWN)
        {
             if (starter==true)
        {
            if ((!checker(b4,b8,b12,b16,b3,b7,b11,b15,b2,b6,b10,b14,b1,b5,b9,b13))&&xxx==false)
            semaphore=false;
            saveButtons();
            logic(b4,b8,b12,b16);
            logic(b3,b7,b11,b15);
            logic(b2,b6,b10,b14);
            logic(b1,b5,b9,b13);
            txtscore.setText(String.valueOf(score));

           

            if (semaphore==false)
            {
                filler();
                semaphore=true;
            }
            colorcombination();
        }      
        }
       
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
